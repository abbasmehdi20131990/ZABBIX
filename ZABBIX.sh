#!bin/bash
##https://computingforgeeks.com/how-to-install-zabbix-server-on-ubuntu/
#Step 1: Install and Configure Apache httpd server
sudo apt update && sudo apt -y full-upgrade
sudo apt install apache2 -y
sudo useradd --no-create-home --shell /bin/false zabbix
sudo rm -rf /etc/apache2/conf-enabled/security.conf
sudo cp -r security.conf /etc/apache2/conf-enabled
sudo rm -rf /etc/apache2/apache2.conf
sudo cp -r apache2.conf /etc/apache2/
sudo systemctl restart apache2
sudo ufw allow http
sudo ufw allow https
#Step 2: Install PHP and required modules
sudo apt install php php-cgi php-common libapache2-mod-php php-mbstring php-net-socket php-gd php-xml-util php-mysql php-bcmath -y
sudo  a2enconf php7.*-cgi
sudo rm -rf /etc/php/*/apache2/php.ini
sudo cp -r php.ini //etc/php/*/apache2/
sudo systemctl daemon-reload
sudo systemctl restart apache2
sudo apt update
sudo apt install mariadb-server -y
sudo mysql -s <<EOF
CREATE DATABASE zabbix character set utf8 collate utf8_bin;;
GRANT ALL PRIVILEGES ON zabbix.* TO zabbix@'localhost' IDENTIFIED BY 'StronDBPassw0rd';
FLUSH PRIVILEGES;
exit
EOF
wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb
sudo apt install ./zabbix-release_5.0-1+focal_all.deb  -y
sudo apt update
sudo apt install zabbix-agent zabbix-server-mysql php-mysql zabbix-frontend-php -y
sudo apt-cache policy zabbix-server-mysql 
#sudo su -
sudo zcat /usr/share/doc/zabbix-proxy-mysql/schema.sql.gz | mysql -uzabbix -p 'StronDBPassw0rd'
sudo rm -rf /etc/zabbix/zabbix_server.conf
sudo cp -r zabbix_server.conf /etc/zabbix/
sudo chown -R zabbix:zabbix /etc/zabbix/
sudo systemctl daemon-reload
sudo systemctl restart zabbix-server
sudo cp -fr php2.ini /etc/php/*/apache2/php.ini
sudo rm -rf /etc/zabbix/zabbix_agentd.conf
sudo cp -rf zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
sudo systemctl restart zabbix-server zabbix-agent apache2
sudo ufw allow proto tcp from any to any port 10050,10051
sudo echo "Access “http://(Zabbix server’s hostname or IP address)/zabbix/”  to begin Zabbix"



